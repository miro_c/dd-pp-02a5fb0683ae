
export const COLOR = {
   color_white: '#ffffff',
   color_gray: '#4f4f4f',
   color_black: '#000000',
   color_red: '#ff0000',
   color_gray_light: '#787878',
   color_background: '#f5f5f5',
   color_green: '#00bd55',
   color_background_main: '#f2faff',
   color_background_splash: '#4c4482',
   color_footer_bg: '#dde7ed',
   color_app_bar: '#4DB6AC',
   color_search_active: '#00897B',
   color_white_alpha: 'rgba(255, 255, 255, 0.5)',
   color_app_bar_count: '#004D40',
   color_content_bg: '#f5f5f5',
   color_material_green: '#1DE9B6',
   color_material_green_dark: '#00BFA5',
}

export const TEXT_SIZE = {
   size_small: '0.5rem',
   size_medium: '1.0rem',
   size_normal: '1.2rem',
   size_normal_mobile: '0.8rem',
   size_large: '1.5rem',
   size_xlarge: '2rem'
}
