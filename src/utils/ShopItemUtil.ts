import { ShopItem } from "../model/ShopItem";
import { User } from "../model/User";

export const getSumPrice = (list: ShopItem[]): number => {
  const priceSum = list.reduce((total, item) => {
    return total + Number(item!.itemPrice!);
  }, 0);

  return priceSum;
};

export const getFilteredShopObject = (list: ShopItem[]) => {
  const shopList = [
    ...new Set(
      list.map((item) => {
        return item.itemTitle;
      }, 0)
    ),
  ];

  const reduceList = shopList.map((title) => {
    const numberOfItem = list.filter((shopItem) => {
      return shopItem.itemTitle === title;
    }).length;

    const sumPrice = list
      .filter((shopItem) => {
        return shopItem.itemTitle === title;
      })
      .reduce((tot, cartItem) => {
        return tot + Number(cartItem.itemPrice!);
      }, 0);

    return {
      itemId: 0,
      itemTitle: title,
      itemPrice: sumPrice.toFixed(2),
      itemCount: numberOfItem,
    };
  }, 0);

  return reduceList;
};

export const getUpdatedShopList = (user: User, list: ShopItem[]) => {
  user?.shopItemList?.forEach((shopItem) => {
    return list.filter((shopItemSecond) => {
      if (shopItemSecond.itemTitle === shopItem.itemTitle) {
        shopItemSecond.itemCount = shopItem.itemCount;
      }
      return shopItemSecond;
    });
  });
};
