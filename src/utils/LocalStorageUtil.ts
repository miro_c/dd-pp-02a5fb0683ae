import { User } from '../model/User';
import { ShopItem } from '../model/ShopItem';
import { v4 as uuidv4 } from 'uuid';

export const saveToLocalStorage = (key: string, uniqueId: string) => {
  window.localStorage.setItem(key, JSON.stringify(uniqueId));
};

export const getFromLocalStorage = (key: string): string | null => {
  const userId = window.localStorage.getItem(key);
  return userId;
};

export const saveUserToLocalStorage = (key: string, user: User | null) => {
  window.localStorage.setItem(key, JSON.stringify(user));
};

export const getUserFromLocalStorage = (key: string): User | null => {
  let userObject = null;

  const userString = window.localStorage.getItem(key);
  if (userString) {
    userObject = JSON.parse(userString) as User;
  }

  return userObject;
};

export const isDifferent = (
  userA: ShopItem[] | undefined,
  userB: ShopItem[] | undefined
): boolean => JSON.stringify(userA) !== JSON.stringify(userB);

export const constantParam = {
  LOCAL_STORAGE_KEY: "userId",
  USER_KEY: "USER_KEY",
  LOCAL_STORAGE_VALUE: ""
};

export const getUniqueUserId = (): string => {
  const uuid = uuidv4().toString();
  return uuid;
};
