import { Dispatch } from "react";
import { User } from "../model/User";
import { ReducerActionEnum } from "../redux-store/ShopItemReducer";

export const dispatchShopItemAction = async (
  action_type: ReducerActionEnum,
  dispatch: Dispatch<any>,
  stateObject: User | null
) => {
  dispatch({
    type: action_type,
    payload: {
      ...stateObject
    },
  });
};
