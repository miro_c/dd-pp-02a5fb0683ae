import { combineReducers } from "redux";
import { ShopItemReducer } from "./ShopItemReducer";

export const rootReducer = combineReducers({
  shopUser: ShopItemReducer,
});

export type AppState = ReturnType<typeof rootReducer>;
