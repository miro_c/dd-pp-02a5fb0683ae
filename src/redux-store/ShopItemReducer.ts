import { User } from "../model/User";

export enum ReducerActionEnum {
  ACTION_CREATE = "ACTION_CREATE",
  ACTION_INCREMENT = "ACTION_INCREMENT",
  ACTION_DECREMENT = "ACTION_DECREMENT",
  ACTION_CURRENT = "ACTION_CURRENT",
}

export interface ItemAction {
  type: ReducerActionEnum;
  payload: User | null;
}

export const ShopItemReducer = (
  state: User | null = null,
  action: ItemAction
): User | null => {
  switch (action.type) {
    case ReducerActionEnum.ACTION_CREATE:
        return action.payload;

    case ReducerActionEnum.ACTION_INCREMENT:
      return action.payload;

    case ReducerActionEnum.ACTION_DECREMENT:
      return action.payload;

    case ReducerActionEnum.ACTION_CURRENT:
      return action.payload;

    default:
      return state;
  }
};
