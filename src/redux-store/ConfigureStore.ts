import { createStore } from 'redux';
import { rootReducer } from './AppState';

const ConfigureStore = () => {
  return createStore(rootReducer, {});
};

export default ConfigureStore;