import { BrowserRouter, Route, Routes } from "react-router-dom";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { ReducerActionEnum } from "./redux-store/ShopItemReducer";
import { dispatchShopItemAction } from "./redux-store/DispatcherAction";
import {
  saveUserToLocalStorage,
  getUserFromLocalStorage,
  constantParam,
  getUniqueUserId,
} from "./utils/LocalStorageUtil";
import { User } from "./model/User";
import PageSplashScreen from "./pages/PageSplashScreen";
import PageMain from "./pages/PageMain";
import PageCartDetail from "./pages/PageCartDetail";
import PageError from "./pages/PageError";
import { GlobalStyle } from "./AppStyle";

const App = () => {
  const dispatch = useDispatch();
  const cachedUser = getUserFromLocalStorage(constantParam.USER_KEY);

  useEffect(() => {
    // --- Init/Save User ---
    async function shopItemInitialAsync() {
      if (!cachedUser) {
        await dispatchShopItemAction(
          ReducerActionEnum.ACTION_CREATE,
          dispatch,
          createNewUser()
        );
      } else {
        await dispatchShopItemAction(
          ReducerActionEnum.ACTION_CURRENT,
          dispatch,
          cachedUser
        );
      }
    }
    shopItemInitialAsync();
  }, []);

  const createNewUser = (): User => {
    let user = { userId: getUniqueUserId(), shopItemList: [] } as User;
    saveUserToLocalStorage(constantParam.USER_KEY, user);
    return user;
  };

  return (
    <>
      <GlobalStyle />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<PageSplashScreen />} />
          <Route path="/items" element={<PageMain />} />
          <Route path="/card" element={<PageCartDetail />} />
          <Route path="*" element={<PageError />} />
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default App;
