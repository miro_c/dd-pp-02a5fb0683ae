import styled, { keyframes } from 'styled-components';
import { COLOR } from '../constants/ConstantsCss';

const ScaleInAnimation = keyframes`
  0% {
      opacity: 0;
      transform: scale(0.9);
  }
  100% {
      opacity: 1;
      transform: scale(1.0);
   }
`;

export const PageSplashMain = styled.div`
  background: ${COLOR.color_app_bar};
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-align: center;
  animation: ${ScaleInAnimation} ease 0.15s;
  animation-iteration-count: 1;
  animation-fill-mode: initial;
`;

export const SplashLogo = styled.img`
  width:12em;
  height: auto;
  align-items: center;
  justify-content: center;
  animation: ${ScaleInAnimation} ease 0.45s;
  animation-iteration-count: 1;
  animation-fill-mode: initial;
`;
