import { FC } from "react";
import { useSelector } from "react-redux";
import { AppState } from "../redux-store/AppState";
import { MainGrid } from "./PageMainStyle";
import AppBar from "../components/AppBar";
import CardDetail from "../components/CartDetail";
import Footer from "../components/Footer";

const PageCartDetail: FC = () => {
  const shopUser = useSelector((state: AppState) => state.shopUser);

  return (
    <>
      <MainGrid>
        <AppBar
          currentUser={shopUser}
          actionShoppingCardDetailCallback={() => {}}
          onPressEnterTextInputCallback={() => {}}
          onChangeSearchCallback={() => {}}
          submitSearchCallback={() => {}}
        />
        <CardDetail />
        <Footer />
      </MainGrid>
    </>
  );
};
export default PageCartDetail;
