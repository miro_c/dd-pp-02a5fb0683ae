import { FC, useEffect, useState, useCallback, useMemo } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { AppState } from "../redux-store/AppState";
import { ReducerActionEnum } from "../redux-store/ShopItemReducer";
import { dispatchShopItemAction } from "../redux-store/DispatcherAction";
import { ShopItem } from "../model/ShopItem";
import {
  getFilteredShopObject,
  getSumPrice,
  getUpdatedShopList,
} from "../utils/ShopItemUtil";
import axios from "axios";
import AppBar from "../components/AppBar";
import MainContent from "../components/MainContent";
import Footer from "../components/Footer";
import { MainGrid } from "./PageMainStyle";

const PageMain: FC = () => {
  const [itemList, setItemList] = useState<ShopItem[]>([]);
  const [itemListFiltered, setItemListFiltered] = useState<ShopItem[]>([]);
  const [searchText, setSearchText] = useState<string>();
  const shopUser = useSelector((state: AppState) => state.shopUser);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  // --- Init Shopping list ---
  useEffect(() => {
    axios
      .get("data.json")
      .then((response) => {
        const shopItemList = response.data.items as Array<ShopItem>;
        updateShopListMemo(shopItemList);
      })
      .catch((err) => console.log(err));
  }, []);

  /***************************************************
   *      Enter press event in Search field
   ***************************************************/
  const onPressEnterTextInputCallback = async (
    e: React.KeyboardEvent<HTMLInputElement>
  ) => {
    if (e.code === "Enter" || e.code === "NumpadEnter") {
      const searchTextInput = e.currentTarget.value;
      setSearchText(searchTextInput);
      submitSearchCallback();
    }
  };

  /***************************************************
   *      onChange event in Search field
   ***************************************************/
  const onChangeSearchCallback = async (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    const searchInput = e.target.value ? e.target.value : "";
    setSearchText(searchInput);
  };

  /***************************************************
   *      Submit search text in Search field
   ***************************************************/
  const submitSearchCallback = async () => {
    const itemFound = itemList.find(
      (element) =>
        element.itemTitle?.toLowerCase() === searchText?.toLowerCase()
    );
    let emptyList: ShopItem[] = [];
    if (itemFound != null) {
      emptyList.push(itemFound);
      setItemListFiltered(emptyList);
    } else {
      emptyList = [];
      setItemListFiltered(emptyList);
    }
  };

  /***************************************************
   *      Show Detail of Shopping Card
   ***************************************************/
  const actionShoppingCardDetail = useCallback((): void => {
    if (shopUser) {
      const shopList = shopUser.shopItemList;
      const priceSum = getSumPrice(shopList!).toFixed(2);
      const reducedList = getFilteredShopObject(shopList!);
      navigate("/card", { state: { list: reducedList, total: priceSum } });
    }
  }, [navigate, shopUser]);

  /***************************************************
   *      Increment item in Shopping basket
   ***************************************************/
  const actionIncrement = useCallback(
    (shopItem: ShopItem): void => {
      if (shopItem.itemCount == null) {
        shopItem.itemCount = 0;
      }
      shopItem.itemCount += 1;
      shopUser?.shopItemList?.push(shopItem);
      dispatchShopItemAction(
        // --- Dispatch Increment Action to global redux store ---
        ReducerActionEnum.ACTION_INCREMENT,
        dispatch,
        shopUser
      );
    },
    [dispatch, shopUser]
  );

  /***************************************************
   *      Decrement item from Shopping Basket
   ***************************************************/
  const actionDecrement = useCallback(
    (shopItem: ShopItem): void => {
      shopItem.itemCount -= 1;
      if (shopUser != null) {
        const id = shopUser?.shopItemList?.indexOf(shopItem);
        if (id != null) {
          shopUser?.shopItemList?.splice(id, 1);
        }
      }
      dispatchShopItemAction(
        // --- Dispatch Decrement Action to global redux store ---
        ReducerActionEnum.ACTION_DECREMENT,
        dispatch,
        shopUser
      );
    },
    [dispatch, shopUser]
  );

  const updateShopList = (list: ShopItem[]) => {
    if (shopUser != null) {
      getUpdatedShopList(shopUser, list);
    }
    setItemList(list);
  };
  const updateShopListMemo = useMemo(() => updateShopList, []);

  return (
    <MainGrid>
      <AppBar
        currentUser={shopUser}
        actionShoppingCardDetailCallback={actionShoppingCardDetail}
        onPressEnterTextInputCallback={onPressEnterTextInputCallback}
        onChangeSearchCallback={onChangeSearchCallback}
        submitSearchCallback={submitSearchCallback}
      />
      <MainContent
        productList={itemListFiltered.length > 0 ? itemListFiltered : itemList}
        actionIncrementCallback={actionIncrement}
        actionDecrementCallback={actionDecrement}
      />
      <Footer />
    </MainGrid>
  );
};
export default PageMain;
