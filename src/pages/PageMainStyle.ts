import styled from "styled-components";

export const MainGrid = styled.div`
  width: 100vw;
  display: grid;
  grid-row-gap: 0em;
  grid-column-gap: 0em;
  grid-template-areas:
    "header header header header header"
    "content content content content content"
    "footer footer footer footer footer";
`;

