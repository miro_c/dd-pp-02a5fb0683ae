import { FC } from "react";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import PalleteIc from "../assets/ic_pallete.svg";
import { PageSplashMain, SplashLogo } from "./PageSplashStyle";

const PageSplashScreen: FC = () => {
  const navigate = useNavigate();

  useEffect(() => {
    goToNextPage();
  });

  const goToNextPage = () => {
    setTimeout(() => {
      navigate("/items", { replace: true });
    }, 2000);
  };

  return (
    <>
      <PageSplashMain>
        <SplashLogo src={PalleteIc}/>
      </PageSplashMain>
    </>
  );
};

export default PageSplashScreen;
