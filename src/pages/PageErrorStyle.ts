import styled, { keyframes } from 'styled-components';
import { COLOR, TEXT_SIZE } from '../constants/ConstantsCss';

const ScaleInAnimation = keyframes`
  0% {
      opacity: 0;
      transform: scale(0.9);
  }
  100% {
      opacity: 1;
      transform: scale(1.0);
   }
`;

export const PageErrorMain = styled.div`
  background: ${COLOR.color_red};
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-align: center;
  animation: ${ScaleInAnimation} ease 0.15s;
  animation-iteration-count: 1;
  animation-fill-mode: initial;
`;

export const ErrorText = styled.p`
  color: ${COLOR.color_white};
  font-size: ${TEXT_SIZE.size_xlarge};
  font-weight: 700;
`;