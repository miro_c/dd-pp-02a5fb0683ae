import { FC } from 'react';
import { PageErrorMain, ErrorText } from './PageErrorStyle';

const PageError: FC = () => {
  return (
    <>
      <PageErrorMain>
        <ErrorText>Error :( </ErrorText>
      </PageErrorMain>
    </>
  );
};
export default PageError;
