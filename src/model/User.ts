import { ShopItem } from "./ShopItem";

export interface User {
  userId: string | undefined;
  shopItemList: Array<ShopItem> | undefined;
}
