
export interface ShopItem {
    itemId: number | undefined;
    itemTitle: string | undefined;
    itemPrice: number | undefined;
    itemCount: number;
}