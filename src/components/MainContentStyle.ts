import styled, { createGlobalStyle, keyframes } from "styled-components";
import { COLOR } from "../constants/ConstantsCss";

const ScaleInAnimation = keyframes`
  0% {
      opacity: 0;
      transform: scale(0.9);
  }
  100% {
      opacity: 1;
      transform: scale(1.0);
   }
`;

export const ContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  grid-area: content;
  height: 80vh;
  background-color: ${COLOR.color_content_bg};
  padding: 0.5em;
`;

export const ContentMain = styled.div`
  display: flex;
  flex-direction: row;
  flex-flow: row wrap;
  align-items: left;
  width: 70vw;
  height: auto;
  row-gap: 2em;
  column-gap: 2em;
  padding: 3em 0.5em 0.5em 0.5em;
  overflow: auto;
  animation: ${ScaleInAnimation} ease 0.15s;
  animation-iteration-count: 1;
  animation-fill-mode: initial;
`;

/**********************************************************************
 *
 *                 --- Responsive Style ---
 *
 **********************************************************************/
export const ContentResponsiveStyle = createGlobalStyle`
 
 /*******  Mobile **********/
 @media all and (min-width: 320px) and (max-width: 480px) {
    ${ContentMain}{
      width: 95vw;
      row-gap: 1em;
      padding: 0.5em;
    }
  }

  /*******  Tablet **********/
  @media all and (min-width: 480px) and (max-width: 1024px) {
     ${ContentMain}{
      width: 90vw;
      column-gap: 0.5em;
      row-gap: 1em;
    }
  }

  /*******  PC **********/
  @media screen and (min-width: 1024px) and (max-width: 1920px) {
 
  }
  
`;
