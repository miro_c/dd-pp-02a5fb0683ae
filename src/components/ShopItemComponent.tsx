import { FC } from "react";
import {
  ItemWrapper,
  ItemInfo,
  ItemTitle,
  ItemPrice,
  ButtonWrapper,
  Button,
  ItemCountText,
} from "./ShopItemStyle";
import { ShopItem } from "../model/ShopItem";

interface ShopItemProps {
  shopItem: ShopItem;
  actionIncrementCallback: (shopItem: ShopItem) => void;
  actionDecrementCallback: (shopItem: ShopItem) => void;
}

const ShopItemComponent: FC<ShopItemProps> = ({
  shopItem,
  actionIncrementCallback,
  actionDecrementCallback,
}: ShopItemProps) => {

  const actionIncrement = () => {
    actionIncrementCallback(shopItem); // --- Call actionIncrementCallback in parent container ---
  };

  const actionDecrement = () => {
    if (shopItem.itemCount > 0) {
      actionDecrementCallback(shopItem); // --- Call actionDecrementCallback in parent container ---
    }
  };

  return (
    <>
      <ItemWrapper>
        <ItemInfo>
          <ItemTitle>{shopItem?.itemTitle}</ItemTitle>
          <ItemPrice>{shopItem?.itemPrice} Czk</ItemPrice>
        </ItemInfo>

        <ButtonWrapper>
          <Button onClick={() => actionDecrement()}>-</Button>
          <ItemCountText>{shopItem.itemCount > 0? shopItem.itemCount : 0}</ItemCountText>
          <Button onClick={() => actionIncrement()}>+</Button>
        </ButtonWrapper>
      </ItemWrapper>
    </>
  );
};
export default ShopItemComponent;
