import { FC } from "react";
import {
  ContentWrapper,
  ContentMain,
  ContentResponsiveStyle,
} from "./MainContentStyle";
import ShopItemComponent from "./ShopItemComponent";
import { ShopItem } from "../model/ShopItem";

interface UserProps {
  productList: ShopItem[] | null;
  actionIncrementCallback: (shopItem: ShopItem) => void;
  actionDecrementCallback: (shopItem: ShopItem) => void;
}

const MainContent: FC<UserProps> = ({
  productList,
  actionIncrementCallback,
  actionDecrementCallback
}: UserProps) => {
  
  return (
    <>
      <ContentResponsiveStyle />
      <ContentWrapper>
        <ContentMain>
          {productList?.map((item) => (
            <ShopItemComponent
              key={item.itemId}
              shopItem={item}
              actionIncrementCallback={actionIncrementCallback}
              actionDecrementCallback={actionDecrementCallback}
            />
          ))}
        </ContentMain>
      </ContentWrapper>
    </>
  );
};
export default MainContent;
