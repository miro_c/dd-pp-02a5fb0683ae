import { FC } from "react";
import {
  ContentWrapper,
  ContentMain,
  CartResponsiveStyle,
  CartTableMain,
  CartEmptyTitle,
  Table,
  TableHead,
  TableHeaderLeft,
  TableHeader,
  TableHeaderRight,
  TableBody,
  TableRow,
  TableValue,
  TableValueCenter,
  TableValueRight,
  CartTitle,
  CartSum,
  ButtonWrapper,
  Button,
} from "./CartDetailStyle";
import { useNavigate, useLocation } from "react-router-dom";
import { ShopItem } from "../model/ShopItem";

interface CartProps {
  list: ShopItem[];
  total: number;
}

const CartDetail: FC = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const itemList = location.state as CartProps;

  return (
    <>
    <CartResponsiveStyle/>
      <ContentWrapper>
        <ContentMain>
          <CartTableMain>
            <CartTitle>CART</CartTitle>
            {itemList && itemList.list.length === 0 ? (
              <CartEmptyTitle>Cart is empty</CartEmptyTitle>
            ) : null}
            {itemList && itemList.list.length > 0 && (
              <Table>
                <TableHead>
                  <tr>
                    <TableHeaderLeft>Name</TableHeaderLeft>
                    <TableHeader>Amount</TableHeader>
                    <TableHeaderRight>Price</TableHeaderRight>
                  </tr>
                </TableHead>
                <TableBody>
                  {itemList.list.map((item, index) => (
                    <TableRow key={index}>
                      <TableValue>{item.itemTitle}</TableValue>
                      <TableValueCenter>{item.itemCount}</TableValueCenter>
                      <TableValueRight>{item.itemPrice} Czk</TableValueRight>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            )}
          </CartTableMain>

          <CartSum>{itemList.total > 0 ? `${itemList.total} Czk` : ""}</CartSum>
          <ButtonWrapper>
            <Button onClick={() => navigate(-1)}>Back</Button>
          </ButtonWrapper>
        </ContentMain>
      </ContentWrapper>
    </>
  );
};
export default CartDetail;
