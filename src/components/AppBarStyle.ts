import styled, { createGlobalStyle } from 'styled-components';
import { COLOR, TEXT_SIZE } from '../constants/ConstantsCss';


export const AppBarWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content:center;
  grid-area: header;
  height: 7vh;
  background-color: ${COLOR.color_app_bar};
  padding: 0.5em;
`;

export const AppBarMain = styled.div`
  display: flex;
  flex-direction: row;
  width: 70vw;
  justify-content:center;
`;

export const AppBarTitle = styled.p`
  display: flex;
  align-items: center;
  color: ${COLOR.color_white};
  font-size: ${TEXT_SIZE.size_large};
  font-weight: 400;
`;

export const RightSection = styled.div`
  display: flex;
  margin-left: auto;
`;

export const SearchInputWrapper = styled.div`
  display: flex;
  align-items: center;
  height: auto;
`;

export const SearchMain = styled.div`
  display: flex;
  flex-direction: row;
  width:20em;
  border: 0.15em solid ${COLOR.color_white_alpha};
  border-radius: 0.5em;
  height: auto;
  background-color: transparent;
  padding: 0.2em;
  transition: background-color 0.25s ease-out 10ms;
  &:hover {
    background-color: ${COLOR.color_search_active};
  }
`;

export const InputStyle = createGlobalStyle`
::placeholder {
  color: ${COLOR.color_white};
  font-size: ${TEXT_SIZE.size_medium};
  font-style: italic;
}

& .block{
  margin: 0 auto;
  width:100%;
  max-width: 30em;
}

& .input-res{
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  font-size: ${TEXT_SIZE.size_medium};
  color: ${COLOR.color_white};
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  width: 100%;
  max-width: 30em;
  background-color: transparent;
  border: none;
  padding: 0.4em 0.15em 0.1em 0.5em;
  border-radius: 0.3em;
  box-shadow: none;
  outline: none;
  margin: 0;
  box-sizing: border-box; 
}
`;

export const SearchIcon = styled.img`
  width:2em;
  height: auto;
  cursor: pointer;
  margin-left: auto;
`;

export const BasketIconWrapper = styled.div`
  display: flex;
  align-items: center;
  padding: 0.2em;
  margin-left: 2em;
  cursor: pointer;
`;

export const BasketIcon = styled.img`
  width: 3em;
  height: auto;
`;

export const BasketCountWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${COLOR.color_app_bar_count};
  width:2.3em;
  height: 1.8em;
  padding:0.3em;
  margin-left:-1em;
  margin-top:1.3em;
  border-radius: 30%;
  border: 0.15em solid ${COLOR.color_white};
`;

export const BasketCountText = styled.p`
  color: ${COLOR.color_white};
  font-size: ${TEXT_SIZE.size_medium};
  font-weight: bold;
  justify-content: center;
`;

/**********************************************************************
 * 
 *                 --- Responsive Style ---
 * 
 **********************************************************************/
 export const AppBarResponsiveStyle = createGlobalStyle`

 /*******  Mobile **********/
 @media all and (min-width: 320px) and (max-width: 480px) {
    ${AppBarWrapper}{
      padding: 0em;
    }

    ${AppBarMain} {
      width: 100vw;
    }
  
    ${AppBarTitle} {
      font-size: ${TEXT_SIZE.size_medium};
      margin-left:1em;
    }

    ${SearchMain} {
      width: 95%;
      padding: 0.12em;
    }
    ${SearchIcon}{
      width:1.2em;
    }

    ${BasketIcon} {
      width: 2em;
    }

    ${BasketIconWrapper} {
      margin-left: 0.2em;
      margin-right: 1em;
    }

    ${BasketCountWrapper} {
      width: 1.8em;
      height: 1.4em;
    }
    ${BasketCountText}{
      font-size: 0.7rem;
    }

    & .input-res{
      padding: 0.15em 0.1em 0.1em 0.15em;
      font-size: 0.8rem;
    }

    ::placeholder {
      font-size: 0.8rem;
    }
  
  }

  /*******  Tablet **********/
  @media all and (min-width: 480px) and (max-width: 1024px) {
    ${AppBarWrapper}{
      padding: 0em;
    }
    ${AppBarMain} {
      width: 90vw;
    }
    ${AppBarTitle} {
      font-size: ${TEXT_SIZE.size_medium};
    }

    ${BasketIcon} {
      width: 2em;
    }

    ${BasketIconWrapper} {
      margin-left: 1em;
      margin-right: 0.7em;
    }
  }

  /*******  PC **********/
  @media screen and (min-width: 1024px) and (max-width: 1920px) {
    ${AppBarTitle} {
      font-size: ${TEXT_SIZE.size_normal};
    }
  }
`;
