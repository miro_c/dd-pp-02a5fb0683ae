import styled, { createGlobalStyle } from "styled-components";
import { COLOR, TEXT_SIZE } from "../constants/ConstantsCss";

export const ItemWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 22em;
  height: auto;
  background-color: ${COLOR.color_white};
  padding: 1em;
  justify-content: space-around;
  -webkit-border-radius: 0.8em;
  -moz-border-radius: 0.8em;
  border-radius: 0.7em;
  -webkit-box-shadow: 0px 0px 4px 0px rgba(0, 0, 0, 0.13);
  box-shadow: 0px 0px 4px 0px rgba(0, 0, 0, 0.13);
`;

export const ItemInfo = styled.div`
  display: flex;
  flex-direction: row;
  padding: 1em;
`;

export const ItemTitle = styled.h1`
  color: ${COLOR.color_gray};
  font-size: ${TEXT_SIZE.size_large};
  font-weight: bold;
`;

export const ItemPrice = styled.p`
  color: ${COLOR.color_material_green};
  font-size: ${TEXT_SIZE.size_xlarge};
  font-weight: bold;
  margin-left: auto;
`;

export const ItemCountText = styled.p`
  justify-content: center;
  color: ${COLOR.color_gray_light};
  font-size: ${TEXT_SIZE.size_xlarge};
  font-weight: 400;
  margin-left: 0.5em;
  margin-right: 0.5em;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  width: 100%;
  height: auto;
  padding: 1em;
`;

export const Button = styled.button`
  width:1.2em;
  height: 1.2em;
  margin-top: 0.2em;
  background: ${COLOR.color_material_green};
  color: ${COLOR.color_white};
  font-size: ${TEXT_SIZE.size_large};
  font-style: bold;
  border: none;
  border-radius: 0.25em;
  cursor: pointer;
  justify-content: center;

  transition: background-color 0.15s ease-out 10ms;
  &:hover {
    background-color: ${COLOR.color_material_green_dark};
  }
`;

/**********************************************************************
 *
 *                 --- Responsive Style ---
 *
 **********************************************************************/
export const ItemResponsiveStyle = createGlobalStyle`
 
 /*******  Mobile **********/
 @media all and (min-width: 320px) and (max-width: 480px) {
    ${ItemWrapper}{
      width: 25em;
      height: 10em;
    }
  }

  /*******  Tablet **********/
  @media all and (min-width: 480px) and (max-width: 1024px) {
    ${ItemWrapper}{
      min-width: 20em;
      height: 10em;
    }
  }

  /*******  PC **********/
  @media screen and (min-width: 1024px) and (max-width: 1920px) {
 
  }
`;
