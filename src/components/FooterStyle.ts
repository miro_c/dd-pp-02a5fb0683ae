import styled from 'styled-components';
import { COLOR } from '../constants/ConstantsCss';

export const FooterWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  grid-area: footer;
  height: 13vh;
  background-color: ${COLOR.color_content_bg};
  padding: 0.5em;
`;

export const FooterMain = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 70vw;
  justify-content:center;
`;

export const FooterText = styled.p`
  margin-top: 1.5em;
  color: ${COLOR.color_gray_light};
`;