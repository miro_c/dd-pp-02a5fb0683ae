import { FC, useState } from "react";
import { FooterWrapper, FooterMain, FooterText } from "./FooterStyle";

const Footer: FC = () => {
  const [year] = useState<number>(new Date().getFullYear())

  return (
    <>
      <FooterWrapper>
        <FooterMain>
          <FooterText>Deso &copy; {year}</FooterText>
        </FooterMain>
      </FooterWrapper>
    </>
  );
};
export default Footer;
