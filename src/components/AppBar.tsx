import { FC } from "react";
import {
  AppBarWrapper,
  AppBarMain,
  AppBarTitle,
  SearchInputWrapper,
  SearchMain,
  InputStyle,
  SearchIcon,
  RightSection,
  BasketIconWrapper,
  BasketIcon,
  BasketCountWrapper,
  BasketCountText,
  AppBarResponsiveStyle,
} from "./AppBarStyle";
import IcSearch from "../assets/ic_search.svg";
import IcBasket from "../assets/ic_shopping_basket.svg";
import { User } from "../model/User";

interface AppBarProps {
  currentUser: User | null;
  actionShoppingCardDetailCallback: () => void;
  onPressEnterTextInputCallback: (e: React.KeyboardEvent<HTMLInputElement>) => void;
  onChangeSearchCallback: (e: React.ChangeEvent<HTMLInputElement>) => void;
  submitSearchCallback: () => void;
}

const AppBar: FC<AppBarProps> = ({
  currentUser,
  actionShoppingCardDetailCallback,
  onPressEnterTextInputCallback,
  onChangeSearchCallback,
  submitSearchCallback,
}: AppBarProps) => {
  return (
    <>
      <AppBarResponsiveStyle />
      <AppBarWrapper>
        <AppBarMain>
          <AppBarTitle>Deso eshop</AppBarTitle>
          <RightSection>
            <SearchInputWrapper>
              <SearchMain>
                <InputStyle />
                <div className="block">
                  <input
                    type="text"
                    placeholder="Find product"
                    className="input-res"
                    onChange={onChangeSearchCallback}
                    onKeyPress={onPressEnterTextInputCallback}
                  />
                </div>
                <SearchIcon
                  src={IcSearch}
                  alt="Search icon"
                  onClick={submitSearchCallback}
                />
              </SearchMain>
            </SearchInputWrapper>
            <BasketIconWrapper
              onClick={() => actionShoppingCardDetailCallback()}
            >
              <BasketIcon src={IcBasket} alt="Basket icon" />
              <BasketCountWrapper>
                <BasketCountText>
                  {currentUser?.shopItemList?.length}
                </BasketCountText>
              </BasketCountWrapper>
            </BasketIconWrapper>
          </RightSection>
        </AppBarMain>
      </AppBarWrapper>
    </>
  );
};
export default AppBar;
