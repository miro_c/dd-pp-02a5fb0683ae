import styled, { keyframes, createGlobalStyle } from 'styled-components';
import { COLOR, TEXT_SIZE } from '../constants/ConstantsCss';

const ScaleInAnimation = keyframes`
  0% {
      opacity: 0;
      transform: scale(0.9);
  }
  100% {
      opacity: 1;
      transform: scale(1.0);
   }
`;

export const ContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  grid-area: content;
  height: 80vh;
  background-color: ${COLOR.color_content_bg};
  padding: 3em;
`;

export const ContentMain = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width:60vw;
  min-height: 60vh;
  overflow-y: auto;
  background-color: ${COLOR.color_white};
  padding: 3em;
  -webkit-border-radius: 0.8em;
  -moz-border-radius: 0.8em;
  border-radius: 0.7em;
  -webkit-box-shadow: 0px 0px 7px 0px rgba(0, 0, 0, 0.1);
  box-shadow: 0px 0px 7px 0px rgba(0, 0, 0, 0.2);
  animation: ${ScaleInAnimation} ease 0.15s;
  animation-iteration-count: 1;
  animation-fill-mode: initial;
`;

export const CartTableMain = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const CartEmptyTitle = styled.h1`
  margin-top: 3em;
  margin-bottom: 1em;
  color: ${COLOR.color_gray_light};
  font-weight: 300;
  font-size: ${TEXT_SIZE.size_xlarge};
`; 

export const Table = styled.table`
  width: 100%;
  height: auto;
  margin-top: 1.5em;
  display: flex;
  flex-flow: column;
  border-collapse: separate;
  border-spacing: 0.1em;
  border-top: 0.05em solid rgb(226, 226, 226);
  border-bottom: 0.05em solid rgb(226, 226, 226);
  display:"flex";
  thead {
    display: table;
    table-layout: fixed;
  }
`;

export const TableBody = styled.tbody`
  flex: 1 1 auto;
  display: block;
  overflow-y: auto;
  tr {
    width: 100%;
    display: table;
    table-layout: fixed;
  }
`;

export const TableHead = styled.thead`
  flex: 0 0 auto;
  width: calc(100% - 0.9em);
`;

export const TableRow = styled.tr`
  color: ${COLOR.color_gray};
  font-weight: 400;
`;

export const TableValue = styled.td`
  color: ${COLOR.color_gray_light};
  padding: 0.8em;
  font-size: ${TEXT_SIZE.size_normal};
  font-weight: 400;
`;

export const TableValueCenter = styled(TableValue)`
  text-align: center;
`;

export const TableValueRight = styled(TableValue)`
  text-align: right;
  color: ${COLOR.color_material_green_dark};
`;

export const TableHeader = styled.th`
  color: ${COLOR.color_gray_light};
  font-size: ${TEXT_SIZE.size_large};
  font-weight: 600;
  padding: 1em;
`;

export const TableHeaderLeft = styled(TableHeader)`
  padding: 1em 1em 1em 0.6em;
  text-align: left;
`;

export const TableHeaderRight = styled(TableHeader)`
  padding: 1em 0.2em 1em 1em;
  text-align: right;
`;

export const CartTitle = styled.h1`
   color: ${COLOR.color_gray};
   font-size: ${TEXT_SIZE.size_xlarge};
   font-style: 700;
`;

export const CartSum = styled.h1`
   color: ${COLOR.color_gray};
   font-size: ${TEXT_SIZE.size_large};
   font-style: 400;
   margin-top: 0.5em;
   margin-left: auto;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  width: 100%;
  height: auto;
  padding: 1em;
`;

export const Button = styled.button`
  width:6em;
  height: auto;
  background: ${COLOR.color_material_green};
  color: ${COLOR.color_white};
  font-size: ${TEXT_SIZE.size_normal};
  font-style: 700;
  border: none;
  border-radius: 0.4em;
  cursor: pointer;
  padding: 0.3em;

  transition: background-color 0.15s ease-out 10ms;
  &:hover {
    background-color: ${COLOR.color_material_green_dark};
  }
`;

/**********************************************************************
 * 
 *                 --- Responsive Style ---
 * 
 **********************************************************************/
 export const CartResponsiveStyle = createGlobalStyle`

 /*******  Mobile **********/
 @media all and (min-width: 320px) and (max-width: 480px) {
    ${ContentWrapper}{
       padding: 0.2em;
    }
    ${ContentMain}{
      width:100%;
      padding: 0.5em;
      margin-top:1em;
    }
    ${TableHeader}{
      font-size: ${TEXT_SIZE.size_medium};
      font-weight: bold;
    }
    ${TableValue}{
      font-size: ${TEXT_SIZE.size_medium};
      font-weight: bold;
    }
  }

  /*******  Tablet **********/
  @media all and (min-width: 480px) and (max-width: 1024px) {
    ${ContentMain}{
      width:100%;
    }

  }

  /*******  PC **********/
  @media screen and (min-width: 1024px) and (max-width: 1920px) {
  
  }
`;
